---
title: About Me
layout: page
excerpt: "Who are you again?"
---

<img src="{{ site.url }}/images/me.jpg" alt="Drawing" style="width: 400px;"/>
⟵ Me

Hey there, I'm Bernardo and I'm a Computer Science student. I set up this blog to share
some of the projects I work on, immediately after that, however, my free time was
pulverized. I enjoy running Linux on potato-like computers (and on actual
potatos!), arbitrary precision arithmetics, and weird C bugs. I'm a quantum
computing enthusiast as well.

I did some work on [factorial algorithms][fact] a while back, and I'm working
on [cappy][cappy], [an implementation of Euler's Totient][euler], and an
[Ackermann calculator][ack]. I maintain a [beautifier for Bash][beautysh], and
I'm working on my own Rust-based OS called [Daedalos][daedalos]. I interned at
[Async][async] for a while, and I'm now working for [Banco Maré][mare].

I'm an advocate for Open Source software and freedom of speech. Here are, for
curiosity's sake, some other things I enjoy:

* Music
  * Grieg, Edvard
  * Ravel, Maurice
  * Rachmaninoff, Sergei
  * King Gizzard and The Lizard Wizard
  * Black Flag
  * Death Grips

* Movies
  * Apocalypse Now
  * The Seventh Seal
  * Wild Strawberries
  * The Great Beauty

* Books
  * The Stranger - Albert Camus
  * The World As Will and Representation - Arthur Schopenhauer
  * In Defense of Lost Causes - Slavoj Zizek
  * Buddenbrooks - Thoman Mann

<a href="http://stackexchange.com/users/2377884/bernard-meurer"><img src="http://stackexchange.com/users/flair/2377884.png" width="208" height="58" alt="profile for Bernard Meurer on Stack Exchange, a network of free, community-driven Q&amp;A sites" title="profile for Bernard Meurer on Stack Exchange, a network of free, community-driven Q&amp;A sites" /></a>

[fact]: http://www.luschny.de/math/factorial/SwingIntro.pdf
[cappy]: https://github.com/DanielSank/cappy
[euler]: https://github.com/bemeurer/euler-function
[ack]: https://github.com/bemeurer/multi-ackermann
[beautysh]: https://github.com/bemeurer/beautysh
[daedalos]: https://github.com/bemeurer/daedalos
[async]: http://www.stoq.com.br/
[mare]: https://www.bancomare.com.br/